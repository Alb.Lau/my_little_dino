from flask import Flask, abort, render_template
import requests, random

app = Flask(__name__)

def getDinos():
    response = requests.get('https://allosaurus.delahayeyourself.info/api/dinosaurs')
    return response.json()

def getDinoData(dinoSlug):
    url="https://medusa.delahayeyourself.info/api/dinosaurs/%s" % dinoSlug
    response = requests.get(url)
    return response.json()

def getRandomDinos():
    return random.sample(getDinos(), 3)

@app.route('/')
def index():
    return render_template('index.html', dinos=getDinos())

@app.route('/dino/<dinoSlug>/')
def detailsDino(dinoSlug):
    return render_template('dino.html', dino=getDinoData(dinoSlug), randomDinos=getRandomDinos())
